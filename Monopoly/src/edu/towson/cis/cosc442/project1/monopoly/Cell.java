package edu.towson.cis.cosc442.project1.monopoly;

public abstract class Cell implements IOwnable  {
	private boolean available = true;
	private String name;
	protected Player thisOwner;

	public String getName() {
		return name;
	}

	public Player getTheOwner()  {
		return thisOwner;
	}
	// m
	public int getPrice() {
		return 0;
	}

	public boolean isAvailable() {
		return available;
	}
	
	public abstract boolean playAction(String msg);

	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	void setName(String name) {
		this.name = name;
	}

	public void setTheOwner(Player owner) {
		this.thisOwner = owner;
	}
    
    public String toString() {
        return name;
    }
}
